package cscie55.hw5;

import org.junit.Test;

public class AccountTest {
	
	/**
	 * Make sure some accounts can be created without error
	 */
	@SuppressWarnings("unused")
	@Test
	public void testAccounts() throws ATMException{
		Account one = new Account(1, 100.0f);
		Account two = new Account(2, 5000.0f);
	}
	
	/**
	 * Make sure constructor won't take invalid floats
	 */
	
	@Test(expected=ATMException.class)
	public void testNaNConstruction() throws ATMException{
		@SuppressWarnings("unused")
		Account A = new Account(1, Float.NaN);
	}
	@Test(expected=ATMException.class)
	public void testInfinityBalance() throws ATMException{
		@SuppressWarnings("unused")
		Account A = new Account(1, Float.POSITIVE_INFINITY);
	}
	
	@Test(expected=ATMException.class)
	public void testNegInfinityBalance() throws ATMException{
		@SuppressWarnings("unused")
		Account A = new Account(1, Float.NEGATIVE_INFINITY);
	}
	
	/**
	 * Make sure invalid floats can't be sent to <code>setBalance</code>
	 */
	
	@Test(expected=ATMException.class)
	public void testNaNSet() throws ATMException{
		Account A = new Account(1, 0.0f);
		A.setBalance(Float.NaN);
	}
	
	@Test(expected=ATMException.class)
	public void testInfinitySet() throws ATMException{
		Account A = new Account(1, 0.0f);
		A.setBalance(Float.POSITIVE_INFINITY);
	}
	@Test(expected=ATMException.class)
	public void testNegInfinitySet() throws ATMException{
		Account A = new Account(1, 0.0f);
		A.setBalance(Float.NEGATIVE_INFINITY);
	}
}