package cscie55.hw5;

import static org.junit.Assert.assertEquals;

import java.rmi.RemoteException;

import org.junit.Test;

public class ATMImplTest {
	/*
	 * test deposit(), checkBalance(), and withdraw() for multiple accounts
	 */
	
	/** test getBalance 
	 * @throws ATMException 
	 * @throws RemoteException */
	@Test
	public void getBalanceTest() throws ATMException, RemoteException {
		ATM atm = new ATMImpl();	// this will normally be called remotely but we're just testing
		assertEquals(0.0f,atm.getBalance(1), 0.001f);
		assertEquals(100.0f,atm.getBalance(2), 0.001f);
		assertEquals(500.0f,atm.getBalance(3), 0.001f);
	}
	
	 /** test withdraw
	  * @throws ATMException */
	 @Test
	 public void withdrawTest() throws ATMException, RemoteException {
		 ATMImpl atm = new ATMImpl();
		 float origBalance = atm.getBalance(1);
		 atm.withdraw(1, 1000.0f);
		 float newBalance = atm.getBalance(1);
		 assertEquals(1000.0f, origBalance - newBalance, .000001);
	 }
	 
	/** test deposit
	 * @throws ATMException */
	 @Test
	 public void depositTest() throws RemoteException, ATMException {
		 ATMImpl atm = new ATMImpl();
		 float origBalance = atm.getBalance(1);
		 atm.deposit(1, 1000.0f);
		 float newBalance = atm.getBalance(1);
		 assertEquals(1000.0f, newBalance - origBalance, .000001);
	 }
}