package cscie55.hw5;

import java.rmi.RemoteException;
import org.junit.Test;

public class ATMFactoryImplTest {
	@Test
	public void constructorTest() throws RemoteException {
		// this should make a call to super() which will export the object
		ATMFactory factory = new ATMFactoryImpl();
		factory.getATM();
	}
}