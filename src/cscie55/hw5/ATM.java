package cscie55.hw5;

/**
 * ATM remote interface defines the contract for ATM operations usch as deposit, withdraw, and check balance. As of version 2.0, this is a
 * remote interface, accessible over a network connection.
 * 
 * @author Bret Fontecchio
 * @version 2.0
 * @since January 17, 2013
 * 
 * @see ATMImpl.java
 */

public interface ATM extends java.rmi.Remote {
	public void deposit(int accountNumber, float amount) throws java.rmi.RemoteException;
	public void withdraw(int accountNumber, float amount) throws java.rmi.RemoteException;
	public float getBalance(int accountNumber) throws java.rmi.RemoteException;
}