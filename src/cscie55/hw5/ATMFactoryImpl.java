package cscie55.hw5;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * ATMFactory defines the functionality of a remote stub factory, which serves as an entry point to the remote system. The remote stub can
 * connect to the registered remote objects and use their functionality according to the remote interface(s) implemented by the remote
 * object(s).
 * 
 * @author Bret Fontecchio
 * @version 2.0
 * @since January 17, 2013
 * 
 * @see ATMFactory.java
 */

public class ATMFactoryImpl extends UnicastRemoteObject implements ATMFactory {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -5677668345560547255L;

	protected ATMFactoryImpl() throws RemoteException {
		super();
	}
	
	public ATM getATM() throws RemoteException {
		// Not using security manager
		try {
		    ATMImpl obj = new ATMImpl();
		    // no need to rebind the name because we're returning the object's reference
		    System.out.println("ATM object created and exported");
		    return obj;
		} catch (Exception e) {
		    System.out.println("ATMImpl Exception: " + e.getMessage());
		    e.printStackTrace();
		    throw new RemoteException("Cannot Createand Export Remote ATM");
		}
	}
}