package cscie55.hw5;

/**
 * ATMFactory remote interface defines the contract for giving a remote client a remote stub factory, which serves as an entry point to the
 * remote system. The remote stub can connect to the registered remote objects and use their functionality according to the remote inter-
 * face(s) implemented by the remote object(s).
 * 
 * @author Bret Fontecchio
 * @version 2.0
 * @since January 17, 2013
 * 
 * @see ATMFactoryImpl.java
 */

public interface ATMFactory extends java.rmi.Remote {
	/**
	 * return a remote reference to an ATM instance
	 */
	public ATM getATM() throws java.rmi.RemoteException;
}