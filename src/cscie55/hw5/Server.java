package cscie55.hw5;

import java.rmi.Naming;

/**
 * Server creates ATMFactory remote object, exports it, and registers it to  "//localhost/atmfactory"
 * 
 * @author Bret Fontecchio
 * @version 2.0
 * @since January 17, 2013
 */

public class Server {
	public static void main(String [] args) {
		try {
		    ATMFactory factory = new ATMFactoryImpl();
		    Naming.rebind("//localhost/atmfactory", factory);
		    System.out.println("atmfactory bound in registry");
		} catch (Exception e) {
		    System.out.println("Server Exception: " + e.getMessage());
		    e.printStackTrace();
		}
	}
}