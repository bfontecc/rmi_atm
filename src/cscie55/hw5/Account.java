package cscie55.hw5;

import cscie55.hw5.ATMException;

/**
 * Account stores a user's running balance as a floating point number. As of version 2.0 multiple accounts are implemented and supported.
 * No checks or guarantees are made about uniqueness of accountNumber. That must be done elsewhere.
 * 
 * @author Bret Fontecchio
 * @version 2.0
 * @since January 17, 2013
 */

public class Account {
	
	private int accountNumber;
	private float balance;
	
	public Account(int accountNumber, float balance) throws ATMException {
		if (Float.isInfinite(balance) || Float.isNaN(balance)) {
			throw new ATMException("Impossible account balance.");
		}
		this.balance = balance;
		this.accountNumber = accountNumber;
	}
	
	protected void setBalance(float updated) throws ATMException {
		if (Float.isInfinite(updated) || Float.isNaN(updated)) {
			throw new ATMException("Impossible account balance.");
		}
		balance = updated;
	}
	
	public float getBalance() {
		return balance;
	}
	
	public int getAccountNumber() {
		return accountNumber;
	}
}